[Envoy](https://www.envoyproxy.io/) is an open source edge and service proxy, designed for cloud native applications. 

The scenario aims to demonstrate how to configure Envoy as a proxy, allowing you to forward traffic to different destinations. 

You will learn how to configure two types of proxies. One will forward traffic to external websites and another which will forward traffic to Docker Containers based on the URL path defined. 

Once the Envoy proxy is in place, it can be extended to support load balancing, health checking and metrics.